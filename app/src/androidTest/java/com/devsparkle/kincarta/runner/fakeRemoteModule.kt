package com.devsparkle.kincarta.runner

import com.devsparkle.kincarta.data.remote.RemoteRetrofitBuilder
import com.devsparkle.kincarta.data.local.casestudy.dao.CaseStudyDao
import com.devsparkle.kincarta.data.local.casestudy.repository.LocalCaseStudyRepositoryImpl
import com.devsparkle.kincarta.data.remote.casestudy.repository.RemoteCaseStudyRepositoryImpl
import com.devsparkle.kincarta.data.remote.casestudy.service.CaseStudyService
import com.devsparkle.kincarta.domain.repository.local.LocalCaseStudyRepository
import com.devsparkle.kincarta.domain.repository.remote.RemoteCaseStudyRepository
import org.koin.android.ext.koin.androidContext
import org.koin.core.qualifier.named
import org.koin.dsl.module
import retrofit2.Retrofit


const val KINCARTA_RETROFIT = "KINCARTA_RETROFIT"
const val SERVER_URL = "SERVER_URL"

val fakeRemoteModule = module {

    factory {
        RemoteCaseStudyRepositoryImpl(
            get<CaseStudyService>()
        ) as RemoteCaseStudyRepository
    }

    factory {
        LocalCaseStudyRepositoryImpl(
            get<CaseStudyDao>()
        ) as LocalCaseStudyRepository
    }

    single(named(SERVER_URL)) {
        "http://localhost:8080/"
    }

    single(named(KINCARTA_RETROFIT)) {
        RemoteRetrofitBuilder.createRetrofit(androidContext(), get(named(SERVER_URL)))
    }

    factory {
        getCaseStudyService(
            get<Retrofit>(named(KINCARTA_RETROFIT))
        )
    }

}

private fun getCaseStudyService(retrofit: Retrofit): CaseStudyService =
    retrofit.create(CaseStudyService::class.java)
