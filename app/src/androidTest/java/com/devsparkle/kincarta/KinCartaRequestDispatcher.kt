package com.devsparkle.kincarta

import com.devsparkle.kincarta.util.FileReader.readTestFile
import okhttp3.mockwebserver.Dispatcher
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.RecordedRequest

/**
 * Handles Requests from mock web server
 */
internal class KinCartaRequestDispatcher : Dispatcher() {

    override fun dispatch(request: RecordedRequest): MockResponse {
        if (request.method.equals("POST")) {
            return when (request.path) {
                "/boguspostenpoint" -> {
                    MockResponse().setResponseCode(200)
                        .setBody(
                            readTestFile("two.json")
                        )
                }
                else -> {
                    MockResponse().setResponseCode(404).setBody("{}")
                }
            }


        } else if (request.method.equals("GET")) {

            return when (request.path) {
                "theappbusiness/engineering-challenge/main/endpoints/v1/caseStudies.json" -> {
                    MockResponse().setResponseCode(200)
                        .setBody(readTestFile("two.json"))
                }
                else -> {
                    MockResponse().setResponseCode(404).setBody("{}")
                }
            }
        } else {
            // 3
            return MockResponse().setResponseCode(401).setBody("{}")
        }

    }


}