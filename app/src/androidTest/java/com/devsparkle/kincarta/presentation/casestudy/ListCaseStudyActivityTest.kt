package com.devsparkle.kincarta.presentation.casestudy

import androidx.test.core.app.ActivityScenario
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.IdlingRegistry
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import com.devsparkle.kincarta.data.local.casestudy.dao.CaseStudyDao
import com.devsparkle.kincarta.util.FileReader
import com.devsparkle.kincarta.utils.EspressoIdlingResource
import kotlinx.coroutines.runBlocking
import okhttp3.mockwebserver.Dispatcher
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import okhttp3.mockwebserver.RecordedRequest
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.koin.test.KoinTest
import org.koin.test.inject

@LargeTest
@RunWith(AndroidJUnit4::class)
class ListCaseStudyActivityTest : KoinTest {

    private val mockWebServer = MockWebServer()
    private val caseStudyDao by inject<CaseStudyDao>()

    @Before
    fun init() {
        runBlocking {
            caseStudyDao.deleteCasestudies()
        }
    }


    @Before
    fun setup() {
        mockWebServer.start(8080)
        IdlingRegistry.getInstance().register(EspressoIdlingResource.countingIdlingResource)
    }

    @After
    fun teardown() {
        mockWebServer.shutdown()
        IdlingRegistry.getInstance().unregister(EspressoIdlingResource.countingIdlingResource)
    }


    @Test
    fun testSuccessfulTwoCaseStudiesResponse(): Unit = runBlocking {
        mockWebServer.dispatcher = object : Dispatcher() {
            override fun dispatch(request: RecordedRequest): MockResponse {
                return MockResponse()
                    .setResponseCode(200)
                    .setBody(FileReader.readTestFile("two.json"))
            }
        }

        // Start up List Case Studies screen
        val activityScenario = ActivityScenario.launch(ListCaseStudyActivity::class.java)
        onView(withText("teaser1")).check(ViewAssertions.matches(isDisplayed()))
        onView(withText("teaser2")).check(ViewAssertions.matches(isDisplayed()))
        // Make sure the activity is closed before resetting the db:
        activityScenario.close()
    }


    @Test
    fun testError404Response() {
        mockWebServer.dispatcher = object : Dispatcher() {
            override fun dispatch(request: RecordedRequest): MockResponse {
                return MockResponse()
                    .setResponseCode(404)
                    .setBody(
                        """
                    {
                      "case_studies": []
                    }
                    """.trimIndent()
                    )
            }
        }
        // Start up List Case Studies screen
        val activityScenario = ActivityScenario.launch(ListCaseStudyActivity::class.java)

        onView(withText("KC (error)")).check(ViewAssertions.matches(isDisplayed()))
        // Make sure the activity is closed before resetting the db:
        activityScenario.close()
    }


    /****
     *
     */
    @Test
    fun test204NoContentResponse() {
        mockWebServer.dispatcher = object : Dispatcher() {
            override fun dispatch(request: RecordedRequest): MockResponse {
                return MockResponse()
                    .setResponseCode(204)
                    .setBody(
                        """
                    {
                      "case_studies": []
                    }
                    """.trimIndent()
                    )
            }
        }
        // Start up List Case Studies screen
        val activityScenario = ActivityScenario.launch(ListCaseStudyActivity::class.java)

        onView(withText("KC (error)")).check(ViewAssertions.matches(isDisplayed()))
        // Make sure the activity is closed before resetting the db:
        activityScenario.close()
    }


    /***
     * Test that when one elements in the response
     * ( example teaser node ) is empty it's ignored and not displayed by the application
     */
    @Test
    fun testOneEmptyTeaserResponse(): Unit = runBlocking {
        mockWebServer.dispatcher = object : Dispatcher() {
            override fun dispatch(request: RecordedRequest): MockResponse {
                return MockResponse()
                    .setResponseCode(200)
                    .setBody(FileReader.readTestFile("oneemptyteaser.json"))
            }
        }

        // Start up List Case Studies screen
        val activityScenario = ActivityScenario.launch(ListCaseStudyActivity::class.java)
        onView(withText("teaser1")).check(ViewAssertions.doesNotExist())
        onView(withText("teaser2")).check(ViewAssertions.matches(isDisplayed()))

        // Make sure the activity is closed before resetting the db:
        activityScenario.close()
    }


}


