package com.devsparkle.kincarta.domain.model


data class CaseStudy(
    val teaser: String?,
    val heroImageUrl: String?
)