package com.devsparkle.kincarta.domain.repository.local

import com.devsparkle.kincarta.base.resource.Resource
import com.devsparkle.kincarta.domain.model.CaseStudy

interface LocalCaseStudyRepository {
    suspend fun getCaseStudies():  Resource<List<CaseStudy>>

    /***
     * Delete all previous Case Study
     * And Persist new Case studies
     */
    suspend fun deleteAndPersistCaseStudies(list: List<CaseStudy>)
}