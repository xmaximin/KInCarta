package com.devsparkle.kincarta.domain.use_case

import com.devsparkle.kincarta.base.resource.Resource
import com.devsparkle.kincarta.domain.model.CaseStudy
import com.devsparkle.kincarta.domain.repository.remote.RemoteCaseStudyRepository


open class GetRemoteCaseStudies(
    private val remoteCaseStudyRepository: RemoteCaseStudyRepository
) {
    suspend operator fun invoke(): Resource<List<CaseStudy>> {
        return remoteCaseStudyRepository.getCaseStudies()
    }
}