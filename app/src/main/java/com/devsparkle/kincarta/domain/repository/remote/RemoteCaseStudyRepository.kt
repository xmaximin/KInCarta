package com.devsparkle.kincarta.domain.repository.remote

import com.devsparkle.kincarta.base.resource.Resource
import com.devsparkle.kincarta.domain.model.CaseStudy
import com.devsparkle.kincarta.domain.model.CaseStudyWrapper

interface RemoteCaseStudyRepository {
    suspend fun getCaseStudies(): Resource<List<CaseStudy>>
}