package com.devsparkle.kincarta.domain.use_case

import com.devsparkle.kincarta.base.resource.Resource
import com.devsparkle.kincarta.domain.model.CaseStudy
import com.devsparkle.kincarta.domain.repository.local.LocalCaseStudyRepository


class GetLocalCaseStudies(
    private val localCaseStudyRepository: LocalCaseStudyRepository
) {
    suspend operator fun invoke(): Resource<List<CaseStudy>> {
        return localCaseStudyRepository.getCaseStudies()
    }
}