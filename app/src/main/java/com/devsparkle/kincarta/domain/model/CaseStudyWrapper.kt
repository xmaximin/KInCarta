package com.devsparkle.kincarta.domain.model

data class CaseStudyWrapper(val casestudies: List<CaseStudy>)