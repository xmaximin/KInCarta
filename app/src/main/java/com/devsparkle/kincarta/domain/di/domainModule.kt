package com.devsparkle.kincarta.domain.di

import com.devsparkle.kincarta.domain.use_case.GetLocalCaseStudies
import com.devsparkle.kincarta.domain.use_case.GetRemoteCaseStudies
import com.devsparkle.kincarta.domain.use_case.PersistCaseStudies
import org.koin.dsl.module

val domainModule = module {

    factory {
        GetLocalCaseStudies(get())
    }

    factory {
        GetRemoteCaseStudies(get())
    }

    factory {
        PersistCaseStudies(get())
    }
}
