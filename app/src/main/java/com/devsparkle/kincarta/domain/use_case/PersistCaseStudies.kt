package com.devsparkle.kincarta.domain.use_case


import com.devsparkle.kincarta.domain.model.CaseStudy
import com.devsparkle.kincarta.domain.repository.local.LocalCaseStudyRepository


class PersistCaseStudies(
    private val localCaseStudyRepository: LocalCaseStudyRepository
) {
    suspend operator fun invoke(list: List<CaseStudy>) {
        localCaseStudyRepository.deleteAndPersistCaseStudies(list)
    }
}
