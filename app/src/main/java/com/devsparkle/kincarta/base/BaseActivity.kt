package com.devsparkle.kincarta.base

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.devsparkle.kincarta.utils.ConnectionLiveData

abstract class BaseActivity : AppCompatActivity() {

    lateinit var connectionLiveData: ConnectionLiveData

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        connectionLiveData = ConnectionLiveData(this)
    }

}