package com.devsparkle.kincarta.base

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

abstract class BaseViewModel : ViewModel() {

    val isNetworkAvailable = MutableLiveData<Boolean>()

}