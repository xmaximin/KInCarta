package com.devsparkle.kincarta.app

import android.app.Application
import android.util.Log
import com.devsparkle.kincarta.base.di.baseModule
import com.devsparkle.kincarta.data.di.localDataModule
import com.devsparkle.kincarta.data.di.remoteDataModule
import com.devsparkle.kincarta.domain.di.domainModule
import com.devsparkle.kincarta.presentation.casestudy.di.caseStudyModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.error.KoinAppAlreadyStartedException

class KinCartaApp : Application() {


    override fun onCreate() {
        super.onCreate()
        setupKoin()
    }


    private fun setupKoin() {
        try {
            startKoin {
                androidContext(this@KinCartaApp)
                androidLogger()
                modules(
                    listOf(
                        // shared module
                        baseModule,
                        // data remote and local module
                        localDataModule,
                        remoteDataModule,
                        // dto objects and use cases
                        domainModule,
                        // domain modules
                        caseStudyModule

                    )
                )
            }

        } catch (koinAlreadyStartedException: KoinAppAlreadyStartedException) {
            Log.i(
                "KinCartaApp",
                "KoinAppAlreadyStartedException, should only happen in tests"
            )
        }
    }
}
