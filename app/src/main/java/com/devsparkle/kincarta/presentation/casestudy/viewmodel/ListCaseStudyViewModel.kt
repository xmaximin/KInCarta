package com.devsparkle.kincarta.presentation.casestudy.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.devsparkle.kincarta.base.BaseViewModel
import com.devsparkle.kincarta.base.resource.Resource
import com.devsparkle.kincarta.domain.model.CaseStudy
import com.devsparkle.kincarta.domain.use_case.GetLocalCaseStudies
import com.devsparkle.kincarta.domain.use_case.GetRemoteCaseStudies
import com.devsparkle.kincarta.domain.use_case.PersistCaseStudies
import com.devsparkle.kincarta.utils.wrapEspressoIdlingResource
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

open class ListCaseStudyViewModel(
    private val persistCaseStudies: PersistCaseStudies,
    private val getRemoteCaseStudies: GetRemoteCaseStudies,
    private val getLocalCaseStudies: GetLocalCaseStudies
) : BaseViewModel() {

    private val _caseStudies = MutableLiveData<Resource<List<CaseStudy>>>()
    val caseStudies: LiveData<Resource<List<CaseStudy>>> = _caseStudies

    fun getCaseStudies() {
        viewModelScope.launch {
            if (isNetworkAvailable.value == true) {
                _caseStudies.postValue(Resource.Loading())
                withContext(Dispatchers.IO) {
                    wrapEspressoIdlingResource {
                        val response = getRemoteCaseStudies.invoke()
                        if (response.isNotAnError()) {
                            response.value()?.let { caseStudies ->
                                persistCaseStudies.invoke(caseStudies)
                                _caseStudies.postValue(response)
                            } ?: run {
                                _caseStudies.postValue(Resource.Error())
                            }
                        } else {
                            _caseStudies.postValue(Resource.Error())
                        }
                    }
                }
            } else {
                _caseStudies.postValue(Resource.Loading())
                withContext(Dispatchers.IO) {
                    wrapEspressoIdlingResource {
                        val response = getLocalCaseStudies.invoke()
                        if (response.isNotAnError()) {
                            _caseStudies.postValue(response)
                        } else {
                            _caseStudies.postValue(Resource.Error())
                        }
                    }
                }

            }
        }
    }

}