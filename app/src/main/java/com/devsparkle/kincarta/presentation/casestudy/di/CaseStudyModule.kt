package com.devsparkle.kincarta.presentation.casestudy.di

import com.devsparkle.kincarta.domain.model.CaseStudy
import com.devsparkle.kincarta.presentation.casestudy.adapter.CaseStudyAdapter
import com.devsparkle.kincarta.presentation.casestudy.viewmodel.ListCaseStudyViewModel
import kotlinx.coroutines.CoroutineDispatcher
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val caseStudyModule = module {

    viewModel {
        ListCaseStudyViewModel(get(), get(), get())
    }

    factory { (clickCallback: ((CaseStudy) -> Unit)) ->
        CaseStudyAdapter(
            clickCallback
        )
    }

}
