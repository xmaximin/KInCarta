package com.devsparkle.kincarta.data.remote.casestudy.repository

import com.devsparkle.kincarta.base.resource.Resource
import com.devsparkle.kincarta.data.mapper.toDomain
import com.devsparkle.kincarta.data.remote.casestudy.service.CaseStudyService
import com.devsparkle.kincarta.domain.model.CaseStudy
import com.devsparkle.kincarta.domain.repository.remote.RemoteCaseStudyRepository
import timber.log.Timber

class RemoteCaseStudyRepositoryImpl(private val service: CaseStudyService) :
    RemoteCaseStudyRepository {

    override suspend fun getCaseStudies(): Resource<List<CaseStudy>> {
        return try {
            val result = service.getCaseStudies()
            Resource.of {
                result.toDomain().casestudies.filterNot { it.teaser.isNullOrEmpty() }
            }
        } catch (e: Exception) {
            Timber.e(e, "Error fetching case studies")
            Resource.Error(e)
        }
    }
}