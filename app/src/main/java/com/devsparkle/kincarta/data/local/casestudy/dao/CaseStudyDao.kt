package com.devsparkle.kincarta.data.local.casestudy.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Transaction
import com.devsparkle.kincarta.data.local.casestudy.entities.CaseStudyEntity

/**
 * Data Access Object for the case study table.
 */
@Dao
interface CaseStudyDao {


    /**
     * Only for test right now.
     *
     * @return all case studies.
     */
    @Query("SELECT * FROM casestudies WHERE teaser=:teaser")
    fun findCaseStudiesByTeaser(teaser: String): List<CaseStudyEntity>


    /**
     * Get the list of case studies.
     *
     * @return all case studies.
     */
    @Query("SELECT * FROM casestudies")
    fun getCaseStudies(): List<CaseStudyEntity>

    @Transaction
    suspend fun deleteAllAndInsertAll(list: List<CaseStudyEntity>) {
        deleteCasestudies()
        list.forEach {
            insertCaseStudy(it)
        }
    }

    /**
     * Insert a case study in the database. If the case study already exists, replace it.
     *
     * @param casestudy the case study to be inserted.
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertCaseStudy(casestudy: CaseStudyEntity)


    /**
     * Delete all cases studies.
     */
    @Query("DELETE FROM casestudies")
    suspend fun deleteCasestudies()


}