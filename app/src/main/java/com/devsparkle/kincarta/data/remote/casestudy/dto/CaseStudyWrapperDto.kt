package com.devsparkle.kincarta.data.remote.casestudy.dto

data class CaseStudyWrapperDto(val case_studies: List<CaseStudyDto>?)