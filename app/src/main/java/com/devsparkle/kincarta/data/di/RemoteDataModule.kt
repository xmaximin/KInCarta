package com.devsparkle.kincarta.data.di


import com.devsparkle.kincarta.data.remote.RemoteRetrofitBuilder
import com.devsparkle.kincarta.data.remote.casestudy.repository.RemoteCaseStudyRepositoryImpl
import com.devsparkle.kincarta.data.remote.casestudy.service.CaseStudyService
import com.devsparkle.kincarta.domain.repository.remote.RemoteCaseStudyRepository
import org.koin.android.ext.koin.androidContext
import org.koin.core.qualifier.named
import org.koin.dsl.module
import retrofit2.Retrofit


const val KINCARTA_RETROFIT = "KINCARTA_RETROFIT"
const val SERVER_URL = "SERVER_URL"

val remoteDataModule = module {


    single(named(SERVER_URL)) {
        //BuildConfig.API_URL
        "https://raw.githubusercontent.com/"
    }

    single(named(KINCARTA_RETROFIT)) {
        RemoteRetrofitBuilder.createRetrofit(androidContext(), get<String>(named(SERVER_URL)))
    }


    factory {
        RemoteCaseStudyRepositoryImpl(
            get()
        ) as RemoteCaseStudyRepository
    }


    factory {
        getCaseStudyService(
            get(named(KINCARTA_RETROFIT))
        )
    }

}

private fun getCaseStudyService(retrofit: Retrofit): CaseStudyService =
    retrofit.create(CaseStudyService::class.java)

