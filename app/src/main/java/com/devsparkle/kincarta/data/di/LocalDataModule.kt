package com.devsparkle.kincarta.data.di

import android.content.Context
import androidx.preference.PreferenceManager
import androidx.room.Room
import com.devsparkle.kincarta.data.local.KinCartaDatabase
import com.devsparkle.kincarta.data.local.casestudy.dao.CaseStudyDao
import com.devsparkle.kincarta.data.local.casestudy.repository.LocalCaseStudyRepositoryImpl
import com.devsparkle.kincarta.domain.repository.local.LocalCaseStudyRepository
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module


val localDataModule = module {

    single {
        get<KinCartaDatabase>().caseStudyDao()
    }

    single {
        Room.databaseBuilder(
            androidContext(),
            KinCartaDatabase::class.java,
            KINCARTA_DATABASE
        )
            .fallbackToDestructiveMigration()
            .allowMainThreadQueries()
            .build()
    }

    factory {
        LocalCaseStudyRepositoryImpl(
            get()
        ) as LocalCaseStudyRepository
    }

    single {
        getSharedPreferences(androidContext())
    }
}
const val KINCARTA_DATABASE = "KINCARTA_DATABASE"


private fun getSharedPreferences(context: Context) =
    PreferenceManager.getDefaultSharedPreferences(context)