package com.devsparkle.kincarta.data.local.casestudy.repository

import com.devsparkle.kincarta.base.resource.Resource
import com.devsparkle.kincarta.data.local.casestudy.dao.CaseStudyDao
import com.devsparkle.kincarta.data.mapper.toDomainCaseStudy
import com.devsparkle.kincarta.data.mapper.toEntity
import com.devsparkle.kincarta.domain.model.CaseStudy
import com.devsparkle.kincarta.domain.repository.local.LocalCaseStudyRepository

class LocalCaseStudyRepositoryImpl(private val caseStudyDao: CaseStudyDao) :
    LocalCaseStudyRepository {

    override suspend fun getCaseStudies(): Resource<List<CaseStudy>> {
        return Resource.of {
            caseStudyDao.getCaseStudies().toDomainCaseStudy()
        }
    }

    override suspend fun deleteAndPersistCaseStudies(list: List<CaseStudy>) {
        caseStudyDao.deleteAllAndInsertAll(list.map { it.toEntity() })
    }
}

