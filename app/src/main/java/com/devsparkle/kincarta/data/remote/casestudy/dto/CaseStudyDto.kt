package com.devsparkle.kincarta.data.remote.casestudy.dto


data class CaseStudyDto(
    val hero_image: String?,
    val teaser: String?,
)