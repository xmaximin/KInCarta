package com.devsparkle.kincarta

import com.devsparkle.kincarta.base.di.baseModule
import com.devsparkle.kincarta.data.di.localDataModule
import com.devsparkle.kincarta.data.di.remoteDataModule
import com.devsparkle.kincarta.data.remote.casestudy.dto.CaseStudyDto
import com.devsparkle.kincarta.data.remote.casestudy.dto.CaseStudyWrapperDto
import com.devsparkle.kincarta.data.remote.casestudy.repository.RemoteCaseStudyRepositoryImpl
import com.devsparkle.kincarta.data.remote.casestudy.service.CaseStudyService
import com.devsparkle.kincarta.domain.di.domainModule
import com.devsparkle.kincarta.presentation.casestudy.di.caseStudyModule
import kotlin.test.assertEquals
import kotlin.test.assertNotNull
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.Rule
import org.junit.Test
import org.koin.dsl.module
import org.koin.test.KoinTest
import org.koin.test.KoinTestRule
import org.koin.test.get
import org.koin.test.mock.MockProviderRule
import org.koin.test.mock.declareMock
import org.mockito.Mockito

@ExperimentalCoroutinesApi
class RemoteCaseStudyRepositoryTest : KoinTest {

    @get:Rule
    val koinTestRule = KoinTestRule.create {
        modules(
            module {
                listOf(
                    // shared module
                    baseModule,
                    // data remote and local module
                    localDataModule,
                    remoteDataModule,
                    // dto objects and use cases
                    domainModule,
                    // domain modules
                    caseStudyModule
                )
            })
    }

    @get:Rule
    val mockProvider = MockProviderRule.create { clazz ->
        Mockito.mock(clazz.java)
    }

    @Test
    fun `Remote repository should remove empty teaser text`() = runTest {
        val listCaseStudiesResponse = listOf<CaseStudyDto>(
            CaseStudyDto(teaser = "teaser1", hero_image = "https://via.placeholder.com/150"),
            CaseStudyDto(teaser = null, hero_image = "https://via.placeholder.com/150"),
        )
        val caseStudyService = declareMock<CaseStudyService>()
        assertNotNull(get<CaseStudyService>())

        Mockito.`when`(caseStudyService.getCaseStudies()).thenReturn(
            CaseStudyWrapperDto(case_studies = listCaseStudiesResponse)
        )

        val remoteCaseStudyRepositoryImpl = RemoteCaseStudyRepositoryImpl(caseStudyService)
        val list = remoteCaseStudyRepositoryImpl.getCaseStudies().value()

        list?.let { assertEquals(1, it.size) }
    }

    @Test
    fun `Remote repository should remove empty teaser text with more case studies`() = runTest {
        val listCaseStudiesResponse = listOf<CaseStudyDto>(
            CaseStudyDto(teaser = "teaser1", hero_image = "https://via.placeholder.com/150"),
            CaseStudyDto(teaser = "", hero_image = "https://via.placeholder.com/150"),
            CaseStudyDto(teaser = "teaser3", hero_image = "https://via.placeholder.com/150"),
            CaseStudyDto(teaser = "", hero_image = "https://via.placeholder.com/150"),
            CaseStudyDto(teaser = "teaser1", hero_image = "https://via.placeholder.com/150"),
            CaseStudyDto(teaser = null, hero_image = "https://via.placeholder.com/150"),
        )

        val caseStudyService = declareMock<CaseStudyService>()
        assertNotNull(get<CaseStudyService>())

        Mockito.`when`(caseStudyService.getCaseStudies()).thenReturn(
            CaseStudyWrapperDto(case_studies = listCaseStudiesResponse)
        )

        val remoteCaseStudyRepositoryImpl = RemoteCaseStudyRepositoryImpl(caseStudyService)
        val list = remoteCaseStudyRepositoryImpl.getCaseStudies().value()

        list?.let { assertEquals(3, it.size) }
    }

}

