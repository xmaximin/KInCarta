package com.devsparkle.kincarta

import com.devsparkle.kincarta.data.local.casestudy.entities.CaseStudyEntity
import com.devsparkle.kincarta.domain.model.CaseStudy
import com.github.javafaker.Faker

object TestUtil {

    fun createDatabaseCaseStudies(nb: Int): List<CaseStudyEntity> {
        val list = mutableListOf<CaseStudyEntity>()

        for (i in 0..nb) {
            list.add(
                CaseStudyEntity(
                    teaser = Faker.instance().company().catchPhrase(),
                    coverimageUrl = Faker.instance().internet().image()
                )
            )
        }
        return list
    }

    fun createDomainCaseStudies(nb: Int): List<CaseStudy> {
        val list = mutableListOf<CaseStudy>()

        for (i in 0..nb) {
            list.add(
                CaseStudy(
                    teaser = Faker.instance().company().catchPhrase(),
                    heroImageUrl = Faker.instance().internet().image()
                )
            )
        }
        return list
    }
}