package com.devsparkle.kincarta

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.devsparkle.kincarta.base.di.baseModule
import com.devsparkle.kincarta.base.resource.Resource
import com.devsparkle.kincarta.data.di.localDataModule
import com.devsparkle.kincarta.data.di.remoteDataModule
import com.devsparkle.kincarta.domain.di.domainModule
import com.devsparkle.kincarta.domain.repository.local.LocalCaseStudyRepository
import com.devsparkle.kincarta.domain.repository.remote.RemoteCaseStudyRepository
import com.devsparkle.kincarta.domain.use_case.GetLocalCaseStudies
import com.devsparkle.kincarta.domain.use_case.GetRemoteCaseStudies
import com.devsparkle.kincarta.domain.use_case.PersistCaseStudies
import com.devsparkle.kincarta.presentation.casestudy.di.caseStudyModule
import com.devsparkle.kincarta.presentation.casestudy.viewmodel.ListCaseStudyViewModel
import kotlin.test.assertEquals
import kotlin.test.assertNotEquals
import kotlin.test.assertNotNull
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runCurrent
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.koin.core.context.loadKoinModules
import org.koin.core.context.stopKoin
import org.koin.test.KoinTest
import org.koin.test.get
import org.koin.test.inject
import org.koin.test.mock.MockProviderRule
import org.koin.test.mock.declareMock
import org.mockito.Mockito
import org.robolectric.RobolectricTestRunner


@ExperimentalCoroutinesApi
@RunWith(RobolectricTestRunner::class)
class ListCaseStudyViewModelTest : KoinTest {

//    @ExperimentalCoroutinesApi
//    @get:Rule
//    val coroutineScope = MainCoroutineRule()


    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()


    private val persistCaseStudies: PersistCaseStudies by inject()
    private val getRemoteCaseStudies: GetRemoteCaseStudies by inject()
    private val getLocalCaseStudies: GetLocalCaseStudies by inject()


    @get:Rule
    val mockProvider = MockProviderRule.create { clazz ->
        // Your way to build a Mock here
        // mockkClass(clazz)
        Mockito.mock(clazz.java)
    }

    @Before
    fun setup() {
        loadKoinModules(
            listOf(
                // shared module
                baseModule,
                // data remote and local module
                localDataModule,
                remoteDataModule,
                // dto objects and use cases
                domainModule,
                // domain modules
                caseStudyModule
            )
        )
        Dispatchers.setMain(UnconfinedTestDispatcher())
    }

    /**
     * Close resources
     */
    @After
    fun after() {
        stopKoin()
        Dispatchers.resetMain()
    }


    @Test
    fun `Testing viewmodel with mocked remote repository`() = runTest(UnconfinedTestDispatcher()) {
        val remoteCaseStudyRepository = declareMock<RemoteCaseStudyRepository>()
        assertNotNull(get<RemoteCaseStudyRepository>())

        val localCaseStudyRepository = declareMock<LocalCaseStudyRepository>()
        assertNotNull(get<LocalCaseStudyRepository>())

        val listCaseStudiesResponse = TestUtil.createDomainCaseStudies(4)
        Mockito.`when`(localCaseStudyRepository.getCaseStudies()).thenReturn(
            Resource.of { listCaseStudiesResponse }
        )
        Mockito.`when`(remoteCaseStudyRepository.getCaseStudies()).thenReturn(
            Resource.of { listCaseStudiesResponse }
        )
        val usecase = GetRemoteCaseStudies(remoteCaseStudyRepository)
        val getLocal = GetLocalCaseStudies(localCaseStudyRepository)
        val viewmodel = ListCaseStudyViewModel(
            persistCaseStudies,
            usecase,
            getLocal
        )
        viewmodel.isNetworkAvailable.postValue(false)

        runBlocking {
            viewmodel.getCaseStudies()
            delay(1000)
        }

        assertEquals(listCaseStudiesResponse, viewmodel.caseStudies.value?.value())
        val listCaseStudiesDiffResponse = TestUtil.createDomainCaseStudies(7)
        assertNotEquals(listCaseStudiesDiffResponse, viewmodel.caseStudies.value?.value())
    }


}

