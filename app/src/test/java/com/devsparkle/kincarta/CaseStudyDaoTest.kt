package com.devsparkle.kincarta


import androidx.test.ext.junit.runners.AndroidJUnit4
import com.devsparkle.kincarta.data.di.localDataModule
import com.devsparkle.kincarta.data.local.KinCartaDatabase
import com.devsparkle.kincarta.data.local.casestudy.dao.CaseStudyDao
import com.devsparkle.kincarta.data.local.casestudy.entities.CaseStudyEntity
import io.mockk.mockkClass
import kotlin.test.assertNotEquals
import kotlin.test.assertTrue
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.koin.core.context.loadKoinModules
import org.koin.core.context.stopKoin
import org.koin.test.KoinTest
import org.koin.test.inject
import org.koin.test.mock.MockProviderRule


@ExperimentalCoroutinesApi
@RunWith(AndroidJUnit4::class)
class CaseStudyDaoTest : KoinTest {

    private val kinCartaDatabase: KinCartaDatabase by inject()
    private val caseStudyDao: CaseStudyDao by inject()


    @get:Rule
    val mockProvider = MockProviderRule.create { clazz ->
        // Your way to build a Mock here
        mockkClass(clazz)
    }

    @Before
    fun setup() {
        loadKoinModules(localDataModule)
    }

    /**
     * Close resources
     */
    @After
    fun after() {
        kinCartaDatabase.close()
        stopKoin()
    }


    @Test
    @Throws(Exception::class)
    fun `verify InsertAndSelect Works`() = runTest {
        // PowerMockito.mockStatic(Log::class.java)
        val cs: List<CaseStudyEntity> = TestUtil.createDatabaseCaseStudies(3)
        val savedTeaser = cs[0]
        cs.forEach {
            caseStudyDao.insertCaseStudy(it)
        }

        assertTrue {
            caseStudyDao.findCaseStudiesByTeaser(savedTeaser.teaser).contains(savedTeaser)
        }
    }

    @Test
    @Throws(Exception::class)
    fun `verify DeleteAndSaveNew Works`() = runTest {

        val cs: List<CaseStudyEntity> = TestUtil.createDatabaseCaseStudies(3)
        cs.forEach {
            caseStudyDao.insertCaseStudy(it)
        }
        val othercs: List<CaseStudyEntity> = TestUtil.createDatabaseCaseStudies(3)

        assertNotEquals(cs, othercs)

        caseStudyDao.deleteAllAndInsertAll(othercs)
        assertNotEquals(cs, caseStudyDao.getCaseStudies())

    }
}